from paranoia_app import create_app
import logging

app = create_app()
if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
if __name__ == '__main__':
    app.run( host = '127.0.0.1', port = 5000, use_reloader = False, debug = False )      
