To activate venv in fish:

    . venv/bin/activate.fish


To run the app:

    export FLASK_APP=paranoia_app
    export FLASK_ENV=production
    export CONFIGURATION_MODE=True   ## Set to True when configuring. Set to False to enable module
    flask run


Run monero node:


Run monero-wallet-rpc:

    ./monero-wallet-rpc --daemon-address 127.0.0.1:18081 --rpc-bind-port 18083 \
                        --wallet-dir PATH_TO_WALLET_DIR  --rpc-login RPUSER:RPCPASSWORD  \
                        --log-level 4
    
Important files:

    config.py                    ## contains configurations
    paranoia_app/__init__.py     ## contains more configurations
    paranoia_app/mod_config/     


Create new wallet:

    flask init-wallet


Create pool of 100.000 integrated addresses

    flask init-addresses


Add fee address:

    flask set-fee-address FEEADDRESS


Add rpc credentials:

    flask set-rpc-credentials --rpc-user RCPUSER --rpc-password RPCPASSWORD
