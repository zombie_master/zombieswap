from flask import Flask, render_template, redirect, flash, url_for, \
                  request, session, Blueprint, current_app, g

import logging

log = logging.getLogger('indx')

mod_index = Blueprint( 'index', __name__, url_prefix = '/' )

@mod_index.route( "/", methods = ["GET"] )
def index():  
    log.info("index visited")
    return render_template( "/index/index.html" )


@mod_index.route( "/support", methods = ["GET"] )
def support():
    log.info("suuport visited")
    return render_template( "/index/support.html" )
