import base64
from random import SystemRandom
import logging
from uuid import uuid4
from captcha.image import ImageCaptcha
from flask import session, request, Markup
from paranoia_app.db import get_db

class FlaskCaptcha(object):

    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)
    
    def init_app(self, app):
        """
        Initialize the captcha extension to the given app object.
        """ 
        xargs = {}
        xargs['height'] = 50
        xargs['width']  = 150

        self.image_generator = ImageCaptcha(**xargs)

        def _generate( answer ):
            answer = str( answer )#.zfill( self.digits )        
            image_data = self.image_generator.generate( answer )
            base64_captcha = base64.b64encode( image_data.getvalue() ).decode( "ascii" )
            logging.debug('Generated captcha with answer: ' + answer)
            return Markup( "<img src='{}'>".format( "data:image/png;base64, {}".format( base64_captcha ) ) )        

        app.jinja_env.globals['captcha_generate'] = _generate


def generate_captcha_challenge():
    print ('generating new captcha...')
    captcha_id     = uuid4().hex
    captcha_answer = uuid4().hex[:5]
    print ('captcha_id:', captcha_id)
    print ('captcha_answer:', captcha_answer)
    db = get_db()
    db.execute(
        'INSERT INTO captchas ( captcha_id, captcha_answer ) VALUES (?, ?)',
                              ( captcha_id, captcha_answer) )
    db.commit()
    return captcha_id, captcha_answer


def validate_captcha_challenge( request ):
    error               = None
    post_captcha_id     = None
    post_captcha_answer = None
    post_captcha_user   = None
    
    print ('loading user answer and id from POST')
    post_captcha_user = request.form['captcha_answer']
    post_captcha_id   = request.form['captcha_id']
    print ('post_captcha_id:',   post_captcha_id)
    print ('post_captcha_user:', post_captcha_user)
    
    db = get_db()
    if post_captcha_id is not None:
        print ('loading captcha record from db...')
        captcha_record = db.execute(
            'SELECT * FROM captchas WHERE captcha_id = ?', (post_captcha_id,)  ).fetchone()
        if captcha_record is not None:
            print ('captcha found in db. loading anwser...')
            post_captcha_answer = captcha_record['captcha_answer']
            print ('post_captcha_answer:', post_captcha_answer)
            if post_captcha_answer == post_captcha_user:
                print ('captcha correct! removing from db...')
                db.execute( 'DELETE FROM captchas WHERE captcha_id = ?', (post_captcha_id,))
                db.commit()
                return True, error
            else:
                error = 'wrong captcha'
                print (error)
        else:    
            error = 'no captcha record with this id.'
            print (error)
    else:
        error = 'no captcha_id in request'
        print (error)
    return False, error
