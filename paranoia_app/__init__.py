# Import flask and template operators
import os
from flask import Flask
from uuid import uuid4


def create_app( test_config = None ):
    #script_directory = os.path.dirname(os.path.abspath(__file__))
    #print 'webapp', script_directory

    app = Flask( __name__, instance_relative_config = False ) 
    app.config.from_mapping(
          DATABASE              = os.path.join( app.instance_path, 'paranoia_app.sqlite' ),
          REGISTRATION_KEYRING  = os.path.join( app.instance_path, 'paranoia_app.temp.gpg' ),
          MEMBERS_KEYRING       = os.path.join( app.instance_path, 'paranoia_app.member.gpg' ),
          KEYRING_DIRECTORY     = app.instance_path#,
    )
    
    token_a = uuid4().hex
    token_b = uuid4().hex
    token_admin = token_a + token_b
    app.config.update(
        ADMIN_TOKEN = token_admin
    )
    print ('admin token: ', app.config['ADMIN_TOKEN'])
    
    #print "test_config: ", test_config
    if test_config is None:
        app.config.from_pyfile( '../config.py', silent = False )
    else:
        app.config.from_mapping( test_config )

    try:
        os.makedirs( app.instance_path )
    except OSError:
        pass

    from . import db
    db.init_app( app ) 

    from . import cryptorpc
    cryptorpc.init_app( app )
    
    from . import captchalib
    flaskcap = captchalib.FlaskCaptcha( app )

    #from . import cryptolib
    #cryptolib.init_app( app ) 

    #from .mod_auth_pgp.controllers import mod_auth_pgp as auth_module
    from .mod_auth_simple.controllers import mod_auth_simple as auth_module    
    app.register_blueprint( auth_module )

    # COMMENT THIS BEFORE DEPLOYMENT!!! 
    # ----------------------------------------------------------------$
    from .mod_config.controllers import mod_config as config_module    
    app.register_blueprint( config_module )
    # --------------------------------------------------------------- $

    from .mod_starter.controllers import mod_starter as starter_module
    app.register_blueprint( starter_module )

#    from .mod_profile.controllers import mod_profile as profile_module
#    app.register_blueprint( profile_module )
#    app.add_url_rule( '/', endpoint = 'profile')

    from .mod_admin.controllers import mod_admin as admin_module
    app.register_blueprint( admin_module )

    ## add index module

    from .mod_index.controllers import mod_index as index_module
    app.register_blueprint( index_module )
    app.add_url_rule( '/', endpoint='index' )

    from flask_qrcode import QRcode
    qrcode = QRcode( app )

    return app

