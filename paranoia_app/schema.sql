DROP TABLE IF EXISTS request_locks;
DROP TABLE IF EXISTS unused_addresses;
DROP TABLE IF EXISTS rpc_credentials;
DROP TABLE IF EXISTS rpc_info;
DROP TABLE IF EXISTS captchas;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS rates;
DROP TABLE IF EXISTS btc_addresses;


CREATE TABLE rates(
    rates_id TEXT UNIQUE PRIMARY KEY,
    btc FLOAT NOT NULL,
    xmr FLOAT NOT NULL,
    xb  FLOAT NOT NULL
);

CREATE TABLE orders(
    order_id TEXT UNIQUE PRIMARY KEY,
    payment_id TEXT NOT NULL,
    xmr_deposit_address TEXT NOT NULL,
    xmr_deposit_amount FLOAT NOT NULL,
    xmr_received_amount INTEGER,
    btc_payout_address TEXT NOT NULL,
    btc_payout_amount FLOAT NOT NULL,
    status TEXT NOT NULL,
    expiration_ts TEXT NOT NULL,
    expired BOOLEAN NOT NULL,
    error TEXT NOT NULL,
    btc_tx_id TEXT,
    xmr_tx_id TEXT,
    xmr_tx_unlock_time TEXT,
    xmr_tx_block_height TEXT,
    xb_rate FLOAT NOT NULL
);

CREATE TABLE request_locks (
    request_lock_id TEXT UNIQUE PRIMARY KEY,
    is_locked BOOLEAN NOT NULL
);

CREATE TABLE rpc_credentials(
    RPCUSER     TEXT NOT NULL,
    RPCPASSWORD TEXT NOT NULL
);

CREATE TABLE unused_addresses (
    integrated_address TEXT UNIQUE PRIMARY KEY,
    payment_id TEXT NOT NULL
);

CREATE TABLE btc_addresses (
    btc_address TEXT UNIQUE PRIMARY KEY,
    label TEXT NOT NULL
);

CREATE TABLE rpc_info(
    rpc_record_id TEXT UNIQUE PRIMARY KEY,
    xmr_rpc_block_height INTEGER NOT NULL,
    btc_rpc_block_height INTEGER NOT NULL,
    last_scanned         INTEGER NOT NULL,
    xmr_balance          INTEGER NOT NULL,
    xmr_unlocked_balance INTEGER NOT NULL,
    btc_balance          INTEGER NOT NULL,
    btc_unlocked_balance INTEGER NOT NULL
);

CREATE TABLE captchas (
    captcha_id TEXT UNIQUE PRIMARY KEY,
    captcha_answer TEXT NOT NULL
);



