from .monerorpc.authproxy import AuthServiceProxy, JSONRPCException
from bitcoinrpc.authproxy import AuthServiceProxy as BTCAuthServiceProxy
from bitcoinrpc.authproxy import JSONRPCException as BTCJSONRPCException

from monero.daemon import Daemon
from monero.backends.offline import OfflineWallet, WalletIsOffline
from monero.wallet import Wallet
from monero.backends.jsonrpc import JSONRPCDaemon

from paranoia_app.db import get_db, init_db
import logging

import click
from flask import current_app, g
from flask.cli import with_appcontext

coin = 'xmr'

def get_rpc_credentials():
    db = get_db()
    rpchost = None
    rpcuser = None
    rpcpassword = None

    config_record = db.execute('SELECT * FROM rpc_credentials').fetchone()
    if config_record is not None:
        rpchost     = "127.0.0.1"
        rpcuser     = config_record['RPCUSER']
        rpcpassword = config_record['RPCPASSWORD']
    else:
        print('rpc not configured!')
        raise Exception('RPC NOT CONFIGURED')
    return rpchost, rpcuser, rpcpassword


def get_rpc_wallet_xmr():
    rpchost, rpcuser, rpcpassword = get_rpc_credentials()

    if 'rpc_wallet_xmr' not in g:
        g.rpc_wallet_xmr = AuthServiceProxy( "http://%s:%s@%s:18083/json_rpc"%( rpcuser, rpcpassword, rpchost ), timeout = 360 )
    return g.rpc_wallet_xmr


def get_rpc_daemon_btc():
    rpchost, rpcuser, rpcpassword = get_rpc_credentials()

    if 'rpc_daemon_btc' not in g:
        g.rpc_daemon_btc = BTCAuthServiceProxy("http://%s:%s@%s:8332/"%( rpcuser, rpcpassword, rpchost ) , timeout=360)
    return g.rpc_daemon_btc


def get_rpc_wallet_btc():
    rpchost, rpcuser, rpcpassword = get_rpc_credentials()

    if 'rpc_wallet_btc' not in g:
        g.rpc_wallet_btc = BTCAuthServiceProxy("http://%s:%s@%s:8332/wallet/" % (rpcuser, rpcpassword, rpchost), timeout=360)
    return g.rpc_wallet_btc


def close_rpc_wallet_xmr( e = None ):
    rpc_wallet_xmr = g.pop( 'db', None )

    if rpc_wallet_xmr is not None:
        rpc_wallet_xmr = None


def close_daemon_rpc( e = None ):
    daemon_rpc = g.pop( 'db', None )

    if daemon_rpc is not None:
        daemon_rpc = None


def close_rpc_wallet_btc( e = None ):
    rpc_wallet_btc = g.pop( 'db', None )

    if rpc_wallet_btc is not None:
        rpc_wallet_btc = None


def close_rpc_daemon_btc( e = None ):
    rpc_daemon_btc = g.pop( 'db', None )

    if rpc_daemon_btc is not None:
        rpc_daemon_btc = None


def init_rpc_info():
    db = get_db()
    db.execute('INSERT INTO rpc_info \
                ( xmr_rpc_block_height, last_scanned, xmr_balance, \
                xmr_unlocked_balance, btc_rpc_block_height, btc_balance, \
                btc_unlocked_balance, rpc_record_id )  VALUES (?,?,?,?,?,?,?,?)',
                (0, 0, 0, 0, 0, 0, 0, 0))
    db.commit()


def init_rates():
    db = get_db()
    db.execute('INSERT INTO rates (rates_id, btc, xmr, xb) VALUES (?,?,?,?)', (0, 0.0, 0.0, 0.0))
    db.commit()


def init_wallet_xmr():
    rpc_wallet_xmr = get_rpc_wallet_xmr()
    rpc_wallet_xmr.create_wallet( {'filename':'exchange_xmr', 'language':'English'} )


def init_wallet_btc():
    rpc_wallet_btc = get_rpc_wallet_btc()
    rpc_wallet_btc.createwallet('exchange_btc')


def init_addresses( count ):
    db = get_db()
    rpc = get_rpc_wallet_xmr()
    rpc.open_wallet({'filename':'exchange_xmr'})
    for i in range( count ):
        integrated_address = rpc.make_integrated_address()
        deposit_address = integrated_address['integrated_address']
        payment_id      = integrated_address['payment_id']
        # print (i, payment_id)
        db.execute( 'INSERT INTO unused_addresses ( integrated_address, payment_id ) VALUES (?,?)', ( deposit_address, payment_id ) )
        db.commit()
    print('ok')
    rpc.close_wallet()
    rpc = get_rpc_daemon_btc()
    btc_address = rpc.getnewaddress('main')
    print(f'address {btc_address}')
    db.execute('INSERT INTO btc_addresses ( btc_address, label ) VALUES (?,?)', (btc_address, 'main'))
    db.commit()


def set_last_scanned(height):
    db = get_db()
    h=int(height)
    db.execute('UPDATE rpc_info SET last_scanned = ?', (h,))
    db.commit()
    db.execute('UPDATE funding_pages SET last_scanned = ?', (h,))
    db.commit()


def set_rpc_credentials( RPCUSER, RPCPASSWORD ):
    db = get_db()
    db.execute('DELETE FROM rpc_credentials')
    db.commit()
    db.execute('INSERT INTO rpc_credentials ( RPCUSER, RPCPASSWORD ) VALUES (?,?)', ( RPCUSER, RPCPASSWORD ) )
    db.commit()


def set_lock():
    db = get_db()
    db.execute(
        'INSERT INTO request_locks ( request_lock_id, is_locked ) VALUES (?,?)', ( 0, False ))
    db.commit()


def reset_lock():
    db = get_db()
    db.execute(
        'UPDATE request_locks SET is_locked = ? WHERE request_lock_id = ?', ( False, 0 ))
    db.commit()


@click.command('init-simple')
@with_appcontext
def init_simple_command():
    init_db()
    try:
        init_wallet_xmr()
    except:
        pass
    try:
        init_wallet_btc()
    except:
        pass
    set_rpc_credentials('RPCUSER', 'RPCPASSWORD')
    init_addresses(1000)
    init_rates()
    init_rpc_info()
    set_lock()
    reset_lock()


@click.command('init-wallet-xmr')
@with_appcontext
def init_wallet_xmr_command():
    init_wallet_xmr()
    click.echo('Initialized XMR wallet')


@click.command('init-wallet-btc')
@with_appcontext
def init_wallet_btc_command():
    init_wallet_btc()
    click.echo('Initialized BTC wallet')


@click.command('init-addresses')
@click.option( '--count', required=True, type=int )
@with_appcontext
def init_addresses_command( count ):
    init_addresses( count )
    click.echo('Initialized addresses')


@click.command('set-last-scanned')
@click.option( '--height', required=True, type=int )
@with_appcontext
def set_last_scanned_command( height ):
    print ('setting last scanned')
    set_last_scanned( height )
    click.echo('configured last scanned')


@click.command('set-lock')
@with_appcontext
def set_lock_command():
    set_lock()
    print ('setting request lock record')
    click.echo('configured request lock')


@click.command('reset-lock')
@with_appcontext
def reset_lock_command():
    reset_lock()
    print ('resetting request lock record')
    click.echo('reset request lock')


@click.command('set-rpc-credentials')
@click.option('--rpc-user',     required=True, type=str )
@click.option('--rpc-password', required=True, type=str )
@with_appcontext
def set_rpc_credentials_command( rpc_user, rpc_password ):
    print ('setting rpc credentials')
    set_rpc_credentials( rpc_user, rpc_password )
    click.echo('configured rpc credentials')


def init_app( app ):
    app.teardown_appcontext( close_daemon_rpc )
    app.teardown_appcontext( close_rpc_daemon_btc )
    app.cli.add_command( init_wallet_xmr_command )
    app.cli.add_command( init_wallet_btc_command )
    app.cli.add_command( init_addresses_command )
    app.cli.add_command(init_simple_command)
    ## starter helper methods

    ## IMPORTANT : Comment out the following lines BEFORE deploying
    app.cli.add_command( set_rpc_credentials_command )
    app.cli.add_command( set_lock_command )
    app.cli.add_command( reset_lock_command)
    app.cli.add_command( set_last_scanned_command )

