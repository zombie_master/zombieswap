import functools
from uuid import uuid4
from paranoia_app.db  import get_db
from werkzeug.security import check_password_hash, generate_password_hash
from flask import Flask, render_template, redirect, flash, url_for, \
                  request, session, Blueprint, current_app, g


mod_config = Blueprint( 'config', __name__, url_prefix = '/config' )

# IMPORTANT DO NOT LEAVE WITHOUT DISABLING
@mod_config.route( "/", methods = ["GET","POST"] )
def admin():
    session.clear()
    db = get_db()
    
    if request.method == 'POST':
        RPCUSER     = request.form['RPCUSER']
        RPCPASSWORD = request.form['RPCPASSWORD']
        FEEADDRESS  = request.form['FEEADDRESS']
        db.execute(
            'INSERT INTO configurations ( RPCUSER, RPCPASSWORD, FEEADDRESS ) VALUES (?, ?, ?)',
            ( RPCUSER, RPCPASSWORD, FEEADDRESS )
        )
        db.commit()
    # Regardless of request method, load configuration from database
    config_record = db.execute( 'SELECT * FROM configurations' ).fetchone()
    
    return render_template( "/config/config.html",
                            config_record = config_record )
