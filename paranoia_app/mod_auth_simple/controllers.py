import logging
import functools
from time import time
from uuid import uuid4
from paranoia_app.logger import setup_logger
from paranoia_app.db  import get_db
from paranoia_app.captchalib import generate_captcha_challenge, validate_captcha_challenge
from werkzeug.security import check_password_hash, generate_password_hash
from flask import Flask, render_template, redirect, flash, url_for, request, session, Blueprint, current_app, g


logging.basicConfig( filename = 'auth.log', level = logging.DEBUG, 
                     format = '%(asctime)s : %(name)s : %(message)s', datefmt='%s')
log = logging.getLogger('auth')

def now():
    return int( time()) 

mod_auth_simple = Blueprint( 'auth', __name__, url_prefix = '/auth_simple' )


@mod_auth_simple.route( "/login", methods = ["GET","POST"] )
def login():
    log.debug('[login] entered')
    captcha_id, captcha_answer = generate_captcha_challenge()
    session.clear()
    g.user = None
    page_id = None
    user_record = None

    if request.method == 'POST':
        access_token = request.form['access_token']
        log.info("received access token {}".format(access_token))
        error        = None
        
        challenge_result, error = validate_captcha_challenge( request )
        if challenge_result:
            db = get_db()
            user_records = db.execute('SELECT * FROM simple_members').fetchall()
            valid = False
            for record in user_records:
                if check_password_hash( record['hashed_token'], access_token):
                    user_record = record
                    valid = True
                    break
            if not valid:
                error = 'auth error'
            if error is None:
                session.clear()
                user_id = user_record['user_id']

                campaign = db.execute('SELECT * FROM funding_pages WHERE user_id = ?', (user_id,)).fetchone()
                db.commit()

                page_id = campaign['page_id']

                session_id = uuid4().hex
                db.execute(
                    """UPDATE user_sessions SET (session_id, last_login, has_logged_out) \
                    = (?, ?, ?) WHERE user_id = ?""", ( session_id, now(), False, user_id ) )
                db.commit()

                session['session_id'] = session_id
                log.info( "[login] successfull login for %s", user_id )

                return redirect( url_for( 'xmr.view_campaign', page_id=page_id ) )

        log.info( '[login] %s', error )
        flash(error)

    return render_template('/auth_simple/login.html', 
                               captcha_answer = captcha_answer,
                               captcha_id     = captcha_id  )


@mod_auth_simple.before_app_request
def load_logged_in_user():

    session_id = session.get('session_id')
    log.info( '[before] session_id: %s', session_id )
    
    if session_id is None:
        g.user = None
        session.clear()

    else:
        session_record = get_db().execute(
            'SELECT * FROM user_sessions WHERE session_id = ?', (session_id,)
        ).fetchone()

        if session_record is None:
            g.user = None
            session.clear()

        else:
            timedelta = now() - session_record['last_login'] 
            has_logged_out = session_record['has_logged_out']
            log.debug( '[before] timedelta: %s', timedelta )
            log.debug( '[before] has_logged_out: %s', has_logged_out )
            if ( timedelta > 1800 ) or ( has_logged_out ):
                log.info( '[before] logging out user')
                g.user = None
                session.clear()

            else:
                user_id = session_record['user_id']
                g.user = get_db().execute(
                    'SELECT * FROM simple_members WHERE user_id = ?', (user_id,)
                ).fetchone()
                log.info( '[before] loaded user record from db in g: %s ', str(  g.user['user_id'] ) )
 

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        log.info( '[login required] checking if user is logged in' )
        if g.user is None:
            log.info( '[login required] None found in g. Redirecting to login...' )
            return redirect( url_for( 'auth.login' ) )

        log.info( '[login required] User found in g. Redirecting to target view...' )
        return view(**kwargs)

    return wrapped_view


@mod_auth_simple.route("/logout")
def logout():
    
    session_id = None
    try:
        session_id = session['session_id']
    except Exception as error:
        print ('error: {}'.format( str( error ) ))
        
    logging.info( 'user logout, session_id: %s', session_id )

    if session_id is not None:
        logging.info( 'setting has_logged_out' )
        db = get_db()
        db.execute(
            'UPDATE user_sessions SET has_logged_out = ? WHERE session_id = ?',
            ( True, session_id )
        )
        db.commit()

    session.clear()
    g.user = None
    return redirect(url_for( "auth.login" ))

