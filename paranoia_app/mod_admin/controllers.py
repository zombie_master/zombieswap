import functools
from uuid import uuid4
from paranoia_app.db  import get_db
from werkzeug.security import check_password_hash, generate_password_hash
from flask import Flask, render_template, redirect, flash, url_for, \
                  request, session, Blueprint, current_app, g


mod_admin = Blueprint( 'admin', __name__, url_prefix = '/admin' )

# IMPORTANT DO NOT LEAVE WITHOUT

def short(s):
    return (len(s)-4)*'*'+s[-4::]

@mod_admin.route( "/", methods = ["GET","POST"] )
def admin():
    orders         = None
    request_locks  = None
    unused_address = None

    session.clear()
    db = get_db()

    login_ok = False
    stored_token = current_app.config['ADMIN_TOKEN']
    if request.method == 'POST':
        admin_token = request.form['admin-token']
        print ('stored_token:', stored_token)
        print ('admin_token', admin_token)
        if admin_token == stored_token:
            print ('ok')
            login_ok       = True
            orders         = db.execute( 'SELECT * FROM orders' ).fetchall()
            unused_address = db.execute( 'SELECT * FROM unused_addresses').fetchall()

        order_id = request.form.get('order_id', None)
        if order_id:
            print('order_id', order_id)
            db.execute('DELETE FROM orders WHERE order_id = ?', (order_id,))
            db.commit()
            orders = db.execute('SELECT * FROM orders').fetchall()
            db.commit()
        if unused_address:
            unused_address = len(unused_address)
    return render_template( "/admin/admin.html",
                              unused_address  = unused_address,
                              orders          = orders,
                              request_locks   = request_locks,
                              login_ok        = login_ok )
