import numbers
import random, string, argparse, uuid, os
import requests
import logging
import json
from monero.backends.offline import OfflineWallet, WalletIsOffline
from monero.wallet import Wallet
from decimal import Decimal

from uuid import uuid4
from time import time

from paranoia_app.captchalib import generate_captcha_challenge, validate_captcha_challenge
from paranoia_app.db         import get_db
from paranoia_app.cryptorpc  import get_rpc_wallet_xmr, get_rpc_wallet_btc, get_rpc_daemon_btc
from flask import Flask, render_template, redirect, flash, url_for, \
                  request, session, Blueprint, current_app, g


log_update = logging.getLogger('loop')
log_main   = logging.getLogger('main')

log_update.setLevel(logging.INFO)
log_main.setLevel(logging.INFO)

MAX_LIMIT = 2000000000000
MIN_LIMIT = 100000000000

def now():
    return int( time())

def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
    return abs(a-b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)


mod_starter = Blueprint( 'xmr', __name__, url_prefix = '/xmr' )
@mod_starter.route("/update", methods=["GET"])
def update():
    ## configuration check
    db = get_db()
    request_lock  = db.execute( 'SELECT * FROM request_locks' ).fetchone()
    if request_lock['is_locked']:
        log_update.info('request LOCKED')
        return redirect("/static/error.html")
    else:
        db.execute('UPDATE request_locks SET (is_locked) = (?) WHERE request_lock_id = ?', (True, 0))
        db.commit()
        log_update.info('setting request LOCK')

        get_rates()
        check_wallets()

        rpc_info_record = None
        db = get_db()

        ## RPC info
        stored_height   = None
        blocks_fetched  = None
        xmr_db_last_scanned = None

        rpc_info_record = db.execute( 'SELECT * FROM rpc_info' ).fetchone()
        log_update.info('loading rpc_info from db ')
        #xmr_stored_height    = rpc_info_record['xmr_rpc_block_height']
        xmr_db_last_scanned  = rpc_info_record['last_scanned']
        #xmr_balance          = rpc_info_record['xmr_balance']
        #xmr_unlocked_balance = rpc_info_record['xmr_unlocked_balance']
        #btc_stored_height    = rpc_info_record['btc_rpc_block_height']
        #btc_balance          = rpc_info_record['btc_balance']
        #btc_unlocked_balance = rpc_info_record['btc_unlocked_balance']

        ## Check payments routine (XMR)
        log_update.info('checking payments routine...')
        rpc_wallet_xmr = get_rpc_wallet_xmr()

        log_update.info('trying to refresh the wallet...')
        # rpc_refresh_result = rpc.refresh( {'start_height' : height } )
        rpc_refresh_result = rpc_wallet_xmr.refresh( )

        blocks_fetched = rpc_refresh_result['blocks_fetched']
        log_update.info('refresh results...')
        log_update.info('xmr blocks fetched: %s', blocks_fetched)
        log_update.info('xmr received money: %s', rpc_refresh_result['received_money'])

        log_update.info('storing wallet...')
        rpc_wallet_xmr.store()

        log_update.info('getting height...')
        rpc_get_height_result = rpc_wallet_xmr.get_height()
        xmr_height = rpc_get_height_result['height']

        log_update.info('height: %s', str(xmr_height))

        log_update.info('updating height in db...')
        db.execute( 'UPDATE rpc_info SET xmr_rpc_block_height = ?', ( xmr_height,))
        db.commit()

        if not xmr_db_last_scanned:
            log_update.info(f'last scanned is null, updating last_scanned {xmr_height}')
            db.execute( 'UPDATE rpc_info SET last_scanned = ?', ( xmr_height,))
            db.commit()
            xmr_db_last_scanned = xmr_height

        check_payments_phase(rpc_refresh_result, xmr_height)

        payout_phase()

        print('Check payments routine Done')
        log_update.info('Check payments routine Done')

        db.execute('UPDATE request_locks SET (is_locked) = (?) WHERE request_lock_id = ?', (False, 0))
        db.commit()
        log_update.info('removed request LOCK')
        return render_template("/xmr/ok.html")


def get_rates():
    ##  Get exchange rates
    log_update.info('getting BTC and XMR rates')
    try:
        response = requests.get('https://api.coindesk.com/v1/bpi/currentprice.json')
        data_btc = response.json()
    except Exception as e:
        log_main.info('error getting btc price')

    try:
        response = requests.get('https://api.cryptowat.ch/markets/kraken/xmrusd/price')
        data_xmr = response.json()
    except Exception as e:
        log_main.info('error getting xmr price')

    btc = data_btc["bpi"]["USD"]["rate"].split('.')[0]
    xmr = data_xmr["result"]["price"]
    float_btc = round(1.1 * float(btc.replace(",", "")), 2)
    float_xmr = round(1.1 * float(xmr), 2)
    float_xb  = round(0.9 * (float_xmr / float_btc), 8)
    xb = "{:.5f}".format(float_xb)
    db = get_db()
    db.execute('UPDATE rates SET (btc, xmr, xb) = (?,?,?) WHERE rates_id = ?', (float_btc, float_xmr, float_xb, 0))
    db.commit()
    log_update.info(f'BTC: ${float_btc} XMR: ${float_xmr} XMR/BTC: {float_xb}')
    return float_btc, float_xmr, float_xb



def check_wallets():
    ##  Load wallet (XMR)
    log_update.info('trying to load XMR wallet...')
    rpc_wallet_xmr = get_rpc_wallet_xmr()
    rpc_wallet_xmr.open_wallet({'filename': 'exchange_xmr'})
    rpc_wallet_xmr.auto_refresh({'enable': False})
    log_update.info('~~XMR WALLET LOADED~~')

    ## Load wallet (BTC)
    log_update.info('trying to load BTC wallet...')
    rpc_wallet_btc = get_rpc_wallet_btc()
    rpc_daemon_btc = get_rpc_daemon_btc()
    btc_wallet = False

    log_update.info('trying to unload btc wallet')
    try:
        rpc_daemon_btc.unloadwallet('exchange_btc')
    except Exception as e:
        print(e)

    log_update.info('trying to load btc wallet')
    try:
        rpc_daemon_btc.loadwallet('exchange_btc')
        btc_wallet = True
    except Exception as e:
        print(e)

    if btc_wallet:
        log_update.info("BTC WALLET LOADED")

    ## Check balances
    log_update.info('getting btc balance')
    #btc_balance_spendable = float(rpc_daemon_btc.getbalance("*", 1))
    btc_balance_trusted   = float(rpc_daemon_btc.getbalances()["mine"]["trusted"])
    btc_balance_spendable = btc_balance_trusted

    log_update.info('getting xmr balance')
    xmr_balance_result   = rpc_wallet_xmr.getbalance()
    xmr_balance          = xmr_balance_result['balance']
    xmr_unlocked_balance = xmr_balance_result['unlocked_balance']

    db = get_db()
    db.execute('UPDATE rpc_info SET (btc_unlocked_balance, btc_balance, xmr_unlocked_balance, xmr_balance) = (?,?,?,?) WHERE rpc_record_id = ?',
                                    (btc_balance_spendable, btc_balance_trusted, xmr_unlocked_balance, xmr_balance, 0))
    db.commit()

    log_update.info(f'btc balances spendable {btc_balance_spendable} trusted {btc_balance_trusted}')
    log_update.info(f'XMR balances unlocked {xmr_unlocked_balance} total {xmr_balance}')
    if btc_balance_spendable < 0.01:
        ## TODO: set liquidity to low and show warning to users/admin
        log_update.info('WARNING: LOW LIQUIDITY')
    elif btc_balance_spendable < 0.001:
        ## TODO: set liquidity to insolvent and disable new orders
        log_update.info('WARNING: INSOLVENT')





mod_starter.route("/liquidity", methods=["GET","POST"])
def add_liquidity():
    pass

def check_expired_phase():
    db = get_db()
    now_ts = now()
    unpaid_unexpired_order_records = db.execute('SELECT * FROM orders WHERE status = ? AND expired = ?', ('unpaid',  False)).fetchall()
    for order in unpaid_unexpired_order_records:
        if int(order['expiration_ts']) - now_ts <= 0:
            db.execute('UPDATE orders SET expired = ? WHERE payment_id = ?', (True, order['payment_id']))
            db.commit()
            log_update.info(f'order {order["order_id"]} expired')


def check_payments_phase(rpc_refresh_result, xmr_height):
    check_expired_phase()
    db = get_db()
    if rpc_refresh_result['received_money']:
        log_update.info('N E W payments F O U N D...')
        order_id = None
        status   = None
        xmr_deposit_address = None
        payment_ids = []
        unpaid_order_records = db.execute('SELECT * FROM orders WHERE status = ? AND error = ? AND expired = ?', ('unpaid', 'none', False)).fetchall()
        for order_record in unpaid_order_records:
            payment_ids.append(order_record['payment_id'])

        if len(payment_ids) > 0:
            log_update.info('payment_ids to check: \n%s', str(payment_ids))
            rpc_wallet_xmr = get_rpc_wallet_xmr()
            rpc_get_bulk_payments_result = None
            rpc_get_bulk_payments_result = rpc_wallet_xmr.get_bulk_payments({'payment_ids': payment_ids})
            payment_results = None
            payment_results = rpc_get_bulk_payments_result['payments']
            for payment_result in payment_results:
                payment_id          = payment_result['payment_id'][:16]
                xmr_received_amount = payment_result['amount']
                xmr_tx_id           = payment_result['tx_hash']
                xmr_tx_unlock_time  = payment_result['unlock_time']
                xmr_tx_block_height = payment_result['block_height']  ## block that first confirmed tx
                xmr_confirmations = xmr_height - xmr_tx_block_height

                db.execute(
                    'UPDATE orders SET (status, xmr_received_amount, xmr_tx_id, xmr_tx_unlock_time, xmr_tx_block_height) = (?,?,?,?,?) WHERE payment_id = ?',
                    ('xmr_received', xmr_received_amount, xmr_tx_id, xmr_tx_unlock_time, xmr_tx_block_height,
                     payment_id,)).fetchone()
                db.commit()

                log_update.info(f'tx confirmations: {xmr_confirmations}')
                log_update.info(f'PID:{payment_id} \namount:{xmr_received_amount} \nconfs:{xmr_confirmations} \ntx_hash:{xmr_tx_id} \nunlock_time:{xmr_tx_unlock_time} \ntx_height:{xmr_tx_unlock_time}')

                order_record = db.execute('SELECT * FROM orders WHERE payment_id = ?', (payment_id,)).fetchone()
                order_id            = order_record['order_id']
                btc_payout_address  = order_record['btc_payout_address']
                btc_payout_amount   = order_record['btc_payout_amount']
                xmr_deposit_address = order_record['xmr_deposit_address']
                xmr_deposit_amount  = order_record['xmr_deposit_amount']
                expiration_ts       = order_record['expiration_ts']
                expired             = order_record['expired']

                log_update.info(f'xmr_height: {xmr_height} tx_height: {xmr_tx_block_height}')
                log_update.debug(f'{order_id} {btc_payout_amount} {btc_payout_address} {xmr_deposit_address} {xmr_deposit_amount} {expiration_ts} {expired}')

                if not isclose( float(xmr_received_amount)/10**12, xmr_deposit_amount ):
                    db.execute('UPDATE orders SET error = ? WHERE payment_id = ?', ('xmr_amount_different', payment_id))
                    db.commit()
                    log_update.info('user sent different amount of XMR order_id: {} ({} instead of {})'.format(order_id, float(xmr_received_amount)/10**12, xmr_deposit_amount))
                else:
                    db.execute('UPDATE orders SET status = ? WHERE payment_id = ?', ('xmr_paid', payment_id))
                    db.commit()


def payout_phase():
    db = get_db()
    log_update.info(f'entering payout phase------------')
    payouts = {}
    payout_order_records = db.execute('SELECT * FROM orders WHERE status = ? AND error = ?', ('xmr_paid', 'none')).fetchall()
    if len(payout_order_records) > 0:
        for payout_order in payout_order_records:
            btc_payout_address = payout_order['btc_payout_address']
            btc_payout_amount  = payout_order['btc_payout_amount']
            order_id   = payout_order['order_id']
            payment_id = payout_order['payment_id']
            # payouts.update({str(btc_payout_address):float(btc_payout_amount)})
            # log_update.info(f'adding {btc_payout_amount} BTC to address {btc_payout_address} for order_id: {order_id} to payout')
            btc_tx_result = None
            btc_rpc = get_rpc_daemon_btc()
            try:
                btc_tx_result = btc_rpc.sendtoaddress(
                    btc_payout_address,
                    btc_payout_amount,
                    f"{order_id}",
                    f"{order_id}",
                    False,
                    False,
                    1 )
            except Exception as e:
                log_update.error(e)
                db.execute('UPDATE orders SET error = ? WHERE payment_id = ?', ('btc_not_sent', payment_id))
                db.commit()
                log_update.info('updated error of order {} to "btc_not_sent"'.format(order_id))
                continue
            if btc_tx_result:
                btc_tx_id = btc_tx_result
                db.execute('UPDATE orders SET (status, btc_tx_id) = (?,?) WHERE payment_id = ?', ('completed', btc_tx_id, payment_id))
                db.commit()
                log_update.info('updated status of order {} to "completed"'.format(order_id))
    else:
        log_update.info("no payouts to process")


def check_balance(payout_amount):
    db = get_db()
    rpc_info = db.execute('SELECT * FROM rpc_info').fetchone()
    unlocked_balance = rpc_info['btc_unlocked_balance']
    if Decimal(payout_amount) > Decimal(unlocked_balance):
        log_main.error('not enough balance')
        return False
    else:
        log_main.info('balance check OK')
        return True


@mod_starter.route("/order/", methods=["GET", "POST"])
def create_order():
    db = get_db()
    log_main.info('entering create order')

    rpc_info_record = db.execute('SELECT * FROM rpc_info').fetchone()
    xmr_height = rpc_info_record['xmr_rpc_block_height']

    rates_record = db.execute('SELECT * FROM rates').fetchone()
    btc_rate = rates_record['btc']
    xmr_rate = rates_record['xmr']
    xb_rate  = rates_record['xb']

    if request.method == "GET":
        log_main.info('GET request. preparing CAPTCHA challenge')
        captcha_id, captcha_answer = generate_captcha_challenge()
        session.clear()
        log_main.info('loading main order screen to user')
        return render_template("/xmr/new_order.html", captcha_answer = captcha_answer, captcha_id = captcha_id )

    elif request.method == "POST":
        challenge_result, error = validate_captcha_challenge(request)
        if challenge_result:
            btc_payout_amount  = request.form['btc_amount']
            btc_payout_address = request.form['btc_dst']
            if check_user_input(btc_payout_amount, btc_payout_address):
                btc_payout_amount  = float(request.form['btc_amount'])
                btc_payout_address = str(request.form['btc_dst'])

                log_main.info(f'POST: user requested {btc_payout_amount} to {btc_payout_address} (${btc_rate * btc_payout_amount})')
                if check_balance(btc_payout_amount):
                    order_id = uuid4().hex
                    log_main.info('generating new order')
                    db = get_db()
                    ## check for unused left
                    unused_address_count = len(db.execute('SELECT * FROM unused_addresses').fetchall())
                    if unused_address_count < 1000:
                        ## TODO: create new addresses
                        pass
                    unused_address_record = db.execute('SELECT * FROM unused_addresses').fetchone()
                    xmr_deposit_address = unused_address_record['integrated_address']
                    payment_id          = unused_address_record['payment_id']
                    xmr_deposit_amount  = round(btc_payout_amount / xb_rate, 8)
                    log_main.info(f"xmr deposit amount {xmr_deposit_amount}")
                    db.execute('INSERT INTO orders (xb_rate, btc_payout_address, btc_payout_amount, xmr_deposit_amount, xmr_deposit_address,\
                                                    payment_id, order_id, status, error, expiration_ts, expired) VALUES (?,?,?,?, ?, ?, ?, ?, ?, ?,?)',
                                                    (xb_rate, btc_payout_address, btc_payout_amount, xmr_deposit_amount, xmr_deposit_address,
                                                    payment_id, order_id, 'unpaid', 'none', str(now()+1800), False))
                    db.commit()
                    log_main.info(f'new order: {order_id} {payment_id}| XMR {xmr_deposit_amount} = {btc_payout_amount} / {xb_rate}')

                    log_main.info('deleting record from unused_addresses table')
                    db.execute('DELETE FROM unused_addresses WHERE payment_id = ?', (payment_id,))
                    db.commit()
                    return redirect(f"/xmr/order/{order_id}")
                else:
                    log_main.info('bad balance')
                    return redirect(f"/xmr/order/")
            else:
                log_main.info('bad input')
                return redirect(f"/xmr/order/")
        else:
            log_main.info('wrong captcha')
            return redirect(f"/xmr/order/")
    else:
        log_main.info('weird request method')
        return redirect(f"/xmr/order/")


def check_user_input(amount, address):
    if isinstance(amount, numbers.Real):
        btc_rpc = get_rpc_daemon_btc()
        if btc_rpc.validateaddress(address)['isvalid']:
            return True
        else:
            log_main.error('input validation error: invalid BTC address')
            return False
    else:
        log_main.error('input validation error: not a float')
    return False


@mod_starter.route("/completed_orders/", methods = ["GET"])
def completed_orders():
    db = get_db()
    completed_order_records = db.execute('SELECT btc_payout_amount, xmr_deposit_amount FROM orders WHERE status = ? LIMIT 5', ("complete",)).fetchall()
    db.commit()
    log_main.info('campaign list')
    return render_template("/xmr/completed_orders.html", completed_order_records = completed_order_records)


@mod_starter.route("/order/<order_id>", methods = ["GET"])
def view_order(order_id):
    deposit_address = None
    status = None
    page_id = None

    ## load database
    db = get_db()

    ## check order
    log_main.info('checking order: {}'.format(order_id))
    order_record = db.execute('SELECT * FROM orders WHERE order_id = ?', (order_id,)).fetchone()
    db.commit()

    ## does order exist?
    if order_record is not None:
        xmr_deposit_address = order_record['xmr_deposit_address']
        xmr_deposit_amount  = order_record['xmr_deposit_amount']
        expiration_ts       = order_record['expiration_ts']
        expires_in          = (int( expiration_ts)-now() )/60
    else:
        error = "order not found"
        log_main.error(error)
        return render_template("/static/404.html")



    return render_template("/xmr/view_order.html", order_record = order_record,
                                                   xmr_deposit_amount  = xmr_deposit_amount,
                                                   xmr_deposit_address = xmr_deposit_address,
                                                   expires_in = int(expires_in))




