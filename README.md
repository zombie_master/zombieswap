ZombieSwap is an account-less KYC-free Open Source Monero -> Bitcoin exchange written in python.

The difference with most exchanges is that you choose how many Bitcoin you want to receive instead of how many Monero you want to send, making it an ideal way to pay for something in Bitcoin using only a Monero wallet.

Run your own exchange now!

The project has been inspired by the now defunct XMR.to <3
